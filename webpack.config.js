const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PROD = "production";
const DEV = "development";

const mode = process.env.NODE_ENV === PROD ? PROD : DEV;
const isProd = mode === PROD;

module.exports = {
	mode,
	entry: './src/base.tsx',
	devtool: isProd ? 'source-map' : 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					options: {
						presets: [
							"@babel/preset-env",
							"@babel/preset-react",
							"@babel/preset-typescript",
						],
					},
				},
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					// Translates CSS into CommonJS
					"css-loader",
					// Compiles Sass to CSS
					"sass-loader"
				],
			},
			{
				test: /\.(png|svg|jpg|jpeg|gif)$/i,
				type: 'asset/resource',
			},
		],
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'build'),
		publicPath: '/'
	},
	devServer: {
		historyApiFallback: true,
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: isProd ? "[name]-[contenthash].css" : "[name].css"
		}
		),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, "src", "base.html"),
			favicon: path.join(__dirname, "src", "images", "illustrations","favicon.png")
		}),
	],
};