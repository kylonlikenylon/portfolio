import * as React from "react";
import { animateScroll } from "react-scroll";
import { Routes, Route, useLocation } from "react-router-dom";
import { NavBar } from "./components/nav-bar";
import { Header } from "./components/header";
import { Home } from "./pages/home";
import { Veracity } from "./pages/veracity";
import { BlueCeramics } from "./pages/blue-ceramics";
import { F812 } from "./pages/f812";
import "./app.scss";

export enum HOME_NAV {
	_bio = "bio",
	_projects = "projects",
	_recreation = "recreation",
	_contact = "contact"

};

export enum PROJECT_NAV {
	_overview = "overview",
	_challenge = "challenge",
	_outcome = "outcome",
	_process = "process",
	_conclusion = "conclusion",
	_reflection = "reflection",
	_thanks = "special-thanks",
	_next = "up-next"

};

export enum PROJ_ROUTE {
	_root = "project",
	_veracity = "veracity",
	_blue_ceramics = "blue-ceramics",
	_f812 = "f812"
}

const MOBILE_BREAKPOINT = 450;

export function App() {

	const SCROLL_OFFSET = 224;

	const HOME_NAV_LIST: string[] = [
		HOME_NAV._bio,
		HOME_NAV._projects,
		HOME_NAV._recreation,
		HOME_NAV._contact
	];

	const PROJ_NAV_LIST: string[] = [
		PROJECT_NAV._overview,
		PROJECT_NAV._challenge,
		PROJECT_NAV._outcome,
		PROJECT_NAV._process,
		PROJECT_NAV._conclusion,
		PROJECT_NAV._reflection
	];

	const [navItem, setNavItem] = React.useState(HOME_NAV_LIST[0]);
	const [navList, setNavList] = React.useState(HOME_NAV_LIST);
	const [goesBack, setGoesBack] = React.useState(false);
	const [isMobile, setIsMobile] = React.useState(false);

	const navSelectHandler = (item: string) => {
		setNavItem(item);
	}

	const location = useLocation();

	React.useEffect(() => {	
		// Scroll back to the top
		animateScroll.scrollToTop({ duration: 250 });

		if (location.pathname.includes(PROJ_ROUTE._root)) {
			setNavList(PROJ_NAV_LIST)
			setNavItem(PROJ_NAV_LIST[0])
			setGoesBack(true);
		} else {
			setNavList(HOME_NAV_LIST)
			setNavItem(HOME_NAV_LIST[0])
			setGoesBack(false);
		}

		const resizeHandler = () => {
			if (window.innerWidth <= MOBILE_BREAKPOINT) {
				setIsMobile(true);
			} else {
				setIsMobile(false);
			}
		};

		window.addEventListener("resize", resizeHandler);
		resizeHandler();

        return () => {
            window.removeEventListener("resize", resizeHandler);
        }

	}, [location, window.innerWidth])


	return (
		<section className="content">
			<Header
				goesBack={goesBack}
			/>
			<NavBar
				navList={navList}
				selected={navItem}
				onClick={navSelectHandler}
				scrollOffset={SCROLL_OFFSET}
			/>
			<main>
				<Routes>
					<Route path={"/"}>
						<Route index element={<Home />} />
						<Route path={PROJ_ROUTE._root}>
							<Route path={PROJ_ROUTE._veracity} element={<Veracity isMobile={isMobile}/>} />
							<Route path={PROJ_ROUTE._blue_ceramics} element={<BlueCeramics isMobile={isMobile} />} />
							<Route path={PROJ_ROUTE._f812} element={<F812 isMobile={isMobile}/>} />
						</Route>
					</Route>
				</Routes>
			</main>
		</section>
	);
}