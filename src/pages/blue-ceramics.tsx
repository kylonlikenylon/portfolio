import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { NestedList, Listing } from "../components/nested-list";
import { ProjectLogistic } from "../components/project-logistic";
import { Subsection } from "../components/subsection";
import { ProjectCard } from "../components/project-card";
import { ImgTextDuo } from "../components/img-text-duo";
import { SoloGraphic } from "../components/solo-graphic";
import { IdeaGrid } from "../components/idea-grid";
import Hero from "../images/blue-ceramics/hero.png";
import HeroMobile from "../images/blue-ceramics/hero-mobile.png";
import Cycle from "../images/blue-ceramics/cycle.png";
import Needs from "../images/blue-ceramics/needs.png";
import Stack from "../images/blue-ceramics/stack.png";
import Structure from "../images/blue-ceramics/structure.png";
import Decay from "../images/blue-ceramics/decay.png";
import Tank from "../images/blue-ceramics/tank.png";
import TankMobile from "../images/blue-ceramics/tank-mobile.png";
import Ecosystem from "../images/blue-ceramics/ecosystem.png";
import CarbonSink from "../images/blue-ceramics/carbon-sink.png";
import Bioinspiration from "../images/blue-ceramics/bioinspiration.png";
import BioinspirationMobile from "../images/blue-ceramics/bioinspiration-mobile.png";
import Sunlight from "../images/blue-ceramics/sunlight.png";
import Current from "../images/blue-ceramics/current.png";
import Height from "../images/blue-ceramics/height.png";
import Trap from "../images/blue-ceramics/trap.png";
import Fabrication from "../images/blue-ceramics/fabrication.png";
import Curvature from "../images/blue-ceramics/curvature.png";
import Underwater from "../images/blue-ceramics/underwater.png";
import SedimentTest from "../images/blue-ceramics/sediment-test.png";
import Model from "../images/blue-ceramics/model.png";
import F812Cover from "../images/covers/f8-12-cover.png";

export interface BlueCeramicsProps {
    isMobile: boolean;
}

export function BlueCeramics(props: BlueCeramicsProps) {

    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Rachel Arredondo",
                "Ofri Dar"
            ]
        },
        {
            title: "Morphing Matters Lab",
            items: [
                "Prof. Lining Yao",
                "Dinesh Patel",
                "Jianzhe Gu",
                "Kexin Lu"
            ]
        },
        {
            title: "Marine Experts",
            items: [
                "Dr. Abbey Engelman",
                "Dr. Jesse Jarvis",
                "Jeffery Good",
                "Dr. Emmett Duffy"
            ]
        },
        {
            title: "Hebrew University of Jerusalem",
            items: [
                "Prof. Eran Sharon",
                "Dr. Arielle Blonder",
                "Shira Shoval"
            ]
        },
        {
            title: "Bezalel Academy of Art and Design",
            items: [
                "Noam Dover",
                "Prof. Haim Parnes"
            ]
        },
        {
            title: "Fireborn Studios",
            items: [
                "Dan Vito",
                "Donna Hetrick",
                "Bennett Graves"
            ]
        }
    ]

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <h1>Blue Ceramics</h1>
            <Section
                title={PROJECT_NAV._overview}
            >
                <SoloGraphic
                    imgUrl={props.isMobile ? HeroMobile : Hero}
                    altText={"A school of fish floating above a patch of seagrass meadow"}
                    caption={"Source: Getty Images"}
                    ignoreTopPadding={true}
                />
                <Subsection>
                    <p>
                        Seagrass meadows not only provide food, shelter, and economy to many parts of the world, they are also many times more effective for carbon capture than rainforests. However, their population has been on the decline.
                    </p>
                    <p>
                        In order to conserve this important ecosystem, my team in the Morphing Matters Lab at Carnegie Mellon University designed 4D ceramic structures to aid in seagrass restoration.
                    </p>
                </Subsection>
                <ProjectLogistic
                    roles={
                        [
                            "Research Assistant",
                            "CNC Specialist"
                        ]
                    }
                    timeline={
                        [
                            "June - September 2021 ",
                            "(4 Months)"
                        ]
                    }
                />
            </Section>
            <Section
                title={PROJECT_NAV._challenge}
            >
                <Subsection
                    title={"deadly cycle"}
                >
                    <p>
                        As seagrass meadows degrade, they face 2 major barriers for regeneration—<em>water clarity and current force</em>. To make matters worse, these factors create an insidious feedback loop. Stronger currents increase sediment levels in the water, which in turn kills off seagrass plants and causes stronger currents.
                    </p>
                    <SoloGraphic
                        imgUrl={Cycle}
                        altText={"A graphic depicting the cycle of increased current force that leads to decreased meadow density"}
                        small={true}
                    />
                </Subsection>
                <Subsection
                    title={"holistic lens"}
                >
                    <p>
                        While restoration is the ultimate goal, <em>we'd be remiss not to consider the humans in the process</em>. Many existing restoration methods are cumbersome to deploy and require clean-up, which makes for a slow process. Ideally, restoration can be easily done, and by anyone anywhere in the world.
                    </p>
                    <SoloGraphic
                        imgUrl={Needs}
                        altText={"A graphic depicting construction and deployment as integral parts of restoration"}
                        small={true}
                    />
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
            >
                <Subsection>
                    <p>
                        After months of research and development, we designed a self-deploying modular 4D morphing ceramic structure.
                    </p>
                    <SoloGraphic
                        imgUrl={props.isMobile ? TankMobile : Tank}
                        altText={"Underwater structure prototypes inside a water tank with seagrass attached to their sides"}
                    />
                    <IdeaGrid
                        title={"a closer look"}
                        ideaList={[]}
                    >
                        <ImgTextDuo
                            imgUrl={Stack}
                            altText={"An illustration of the clay pieces stacked up"}
                        >
                            <p>
                                A <em>simple fabrication method</em> creates an easily transportable shape that also disrupts currents.
                            </p>
                        </ImgTextDuo>
                        <ImgTextDuo
                            imgUrl={Structure}
                            altText={"An illustration of the structure — each unit looks like a mountain that's been cut through the middle"}
                            goofy={true}
                        >
                            <p>
                                When the pieces are attached together, they form <em>self-submerging structures</em> with openings to trap sediment.
                            </p>
                        </ImgTextDuo>
                        <ImgTextDuo
                            imgUrl={Decay}
                            altText={"An illustration of broken ceramic pieces to denote decay"}
                        >
                            <p>
                                Over time, the structures will <em>gradually decay into the surrounding environment</em>, leaving no trace of their existence.
                            </p>
                        </ImgTextDuo>
                    </IdeaGrid>
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._process}
            >
                <Subsection
                    title={"first encounter"}
                >
                    <p>
                        This project initially aimed to create structures for coral reef restoration. However, in conversation with marine experts, they redirected our attention to another organism that provides as many benefits to ocean life yet receives far less attention—<em>the humble seagrass</em>.
                    </p>
                    <ImgTextDuo
                        imgUrl={Ecosystem}
                        altText={"A seagrass meadow with a school of fish and seahorse swimming around it"}
                    >
                        <p>
                            Seagrass meadows <em>provide food and shelter for residents of reefs</em>, so they are essential to the health of coastal ecosystems and economies.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={CarbonSink}
                        altText={"A seagrass meadow with shading underneath each plant to show where carbon is stored"}
                        goofy={true}
                    >
                        <p>
                            Recent research has shown that seagrass meadows (along with other coastal ecosystems) are <em>tremendous carbon sinks</em>, beating out even rainforests.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"Bio-Inspiration"}
                >
                    <p>
                        To better understand the structural requirements to foster healthy seagrass environments, we conducted co-design sessions with marine experts.
                    </p>
                    <SoloGraphic
                        imgUrl={props.isMobile ? BioinspirationMobile : Bioinspiration}
                        altText={"Sketches from co-design sessions with marine scientists"}
                    />
                    <p className={"floating-block"}>
                        These collaboration sessions brought forward the most important issues to address for restoration—water clarity and current forces.
                    </p>
                    <ImgTextDuo
                        imgUrl={Sunlight}
                        altText={"Illustration of sunlight piercing through water to reach the seagrass"}
                    >
                        <p>
                            Healthy meadows need clear water that allows sunlight to pass through and nourish the plants.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={Current}
                        altText={"Illustration to show that dense seagrass meadows can buffer against stronger sea currents"}
                        goofy={true}
                    >
                        <p>
                            Dense meadows slow down currents and trap sediment to keep the water clear.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"Shape shifter"}
                >
                    <p>
                        Our design exploration prioritized the re-establishment of a healthy environment to promote meadow regrowth.
                    </p>
                    <ImgTextDuo
                        imgUrl={Height}
                        altText={"Illustration showing that the structures at different heights are more effective against the currents"}
                    >
                        <p>
                            We favored curved shapes for their simplicity and <em>ability to redirect oncoming currents</em>.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={Trap}
                        altText={"Illustration of isometric view of each structure to show that the flared out shape traps sediment"}
                        goofy={true}
                    >
                        <p>
                            Pronounced curvature at the end of each piece ensures that the structure is <em>self-submerging and traps sediment</em>.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"Material World"}
                >
                    <p>
                        One of the goals of this project was to explore scalable production methods. To that point, we created prototypes using digital fabrication techniques and compared them to hand-made samples.
                    </p>
                    <ImgTextDuo
                        imgUrl={Fabrication}
                        altText={"An image that compares shrinkage of clay samples between different fabrication methods"}
                    >
                        <p>
                            We controlled for variables like thickness and orientation to test the effects of each fabrication method on the resulting shape.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={Curvature}
                        altText={"An image of many stacks of samples that were created in the research process"}
                        goofy={true}
                    >
                        <p>
                        As we predicted, <em>CNC fabrication yielded the most consistent results</em> with little deformities, but hand-making was still faster for prototypes.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"moment of truth"}
                >
                    <p>
                        Figuring out the right fabrication method was just the first step—our proposed solution was still contingent on a heaping pile of assumptions. We needed solid proof that this idea will be successful.
                    </p>
                    <ImgTextDuo
                        imgUrl={Underwater}
                        altText={"A rendered image of the structures sitting on the seafloor surrounded by seagrass"}
                    >
                        <p>
                            I built an environment in Unreal Engine for quick feedback from marine experts about the feasibility of the overall idea.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={SedimentTest}
                        altText={"A tank with a water pump on one side and modeled structures on the other. Colored sand on the bottom of the tank shows that the structures are able to trap sediment"}
                        goofy={true}
                    >
                        <p>
                            We also set up a sediment tank and showed that theses structures are able to trap sediment, albeit under controlled conditions.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={Model}
                        altText={"A life-size 3D printed model of what we envision each unit of the structure to look like"}
                    >
                        <p>
                        For the final piece of the puzzle, we created a life-sized model of the structure and demonstrated that fabrication is possible at this scale.
                        </p>
                    </ImgTextDuo>
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._conclusion}
            >
                <p>
                    Where this project left off is by no means the end of the conversation. Climate change requires immediate and collective action, so I am truly grateful to have this opportunity to work on such an impactful project with an incredible team.

                </p>
                <p>
                    I look forward to working on other similar project because I believe nothing is more important than <em>ensuring the propsperity of our world for future generations of every species</em>.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._reflection}
            >
                <p>
                    This experience entrenched my belief that <em>interdisciplinary work is the way forward</em>. Our project stands on the shoulders of countless marine scientists, industrial designers, professors, and researchers from around the world; none of this work would have been possible without their knowledge and expertise.
                </p>
                <p>
                    As such, I want to thank all the experts who volunteered their precious time and energy to collaborate with us. Many thanks to the members of the Morphing Matters Lab for their support, and especially Prof Lining Yao for hosting this project. And special shout-outs to my teammates Rachel and Ofri for bearing with me, even when I was unbearable.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
            >
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._f812}`}
                    title={"F8*12"}
                    description={"Introducing AI ethics into middle and high school curriculum"}
                    imgUrl={F812Cover}
                    altText={"Outline drawing of a human-like robot with dark eyes and golden contour lines"} />
            </Section>
        </React.Fragment>
    );
}