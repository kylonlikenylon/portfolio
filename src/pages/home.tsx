import * as React from "react";
import { Helmet } from "react-helmet";
import { HOME_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { ImgTextDuo } from "../components/img-text-duo";
import { ProjectCard } from "../components/project-card";
import { Subsection } from "../components/subsection";
import { ImgDuo } from "../components/img-duo";
import { ContactInfo } from "../components/contact-info";
import SelfPortrait from "../images/illustrations/self-portrait.png";
import VeracityCover from "../images/covers/veracity-cover.png";
import BlueCeramicsCover from "../images/covers/blue-ceramics-cover.png";
import F812Cover from "../images/covers/f8-12-cover.png";
import CCA from "../images/genarts/cca.png";
import Physarum from "../images/genarts/physarum.png";
import Kitty from "../images/film/kitty.png";
import Osaka from "../images/film/osaka.png";

export function Home() {


    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Theory</title>
            </Helmet>
            <Section
                title={HOME_NAV._bio}
            >
                <ImgTextDuo
                    imgUrl={SelfPortrait}
                    altText="Kylon's self-portrait as a clown in a dunce hat and neck ruffle"
                >
                    <p>
                        Kylon is an engineer by trade and an artist at heart. Most importantly, <em>a lover above anything else</em>.
                    </p>
                    <p>
                    Their work weaves knowledge across disciplines to advance the well-being of all living organisms.
                    </p>
                    <p>
                        Queer af.
                    </p>
                </ImgTextDuo>
            </Section>
            <Section
                title={"projects"}
            >
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._veracity}`}
                    title={"Veracity"}
                    description={"Mitigating the consumption and spread of misinformation "}
                    imgUrl={VeracityCover}
                    altText={"Two clay phones in isometric view showing prototypes of interventions for misinformation on social platforms"} />
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._blue_ceramics}`}
                    title={"Blue Ceramics"}
                    description={"Restoring seagrass meadows with morphing ceramic structures"}
                    imgUrl={BlueCeramicsCover}
                    altText={"Isometric view of mountain-like ceramic structures posed in an underwater scene with marine life and seagrass"} />
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._f812}`}
                    title={"F8*12"}
                    description={"Introducing AI ethics into middle and high school curriculum"}
                    imgUrl={F812Cover}
                    altText={"Outline drawing of a human-like robot with dark eyes and golden contour lines"} />
            </Section>
            <Section
                title={HOME_NAV._recreation}
            >
                <Subsection
                    title={"Generative Art"}
                >
                    <ImgDuo
                        alphaImgUrl={CCA}
                        alphaAltText={"Triangle-dominant geometric shapes generated with cyclic cellular automata algorithm"}
                        deltaImgUrl={Physarum}
                        deltaAltText={"Wavy and wispy white trails generated with physarum algorithm"}
                    />
                </Subsection>
                <Subsection
                    title={"Film Photography"}
                >
                    <ImgDuo
                        alphaImgUrl={Kitty}
                        alphaAltText={"White and gray cat playing with toy at the top of a cat tree"}
                        deltaImgUrl={Osaka}
                        deltaAltText={"Osaka castle emerges among cherry blossom trees"}
                    />
                </Subsection>
            </Section>
            <Section
                title={HOME_NAV._contact}
            >
                <ContactInfo
                    methodList={
                        [
                            {
                                text: "Download Resume",
                                url: "https://drive.google.com/file/d/1tr1zOA8Dr4Cb6dCDRG6TPp44bWZG9WD-/view?usp=sharing"
                            },
                            {
                                text: "Connect on LinkedIn",
                                url: "https://www.linkedin.com/in/kylon-chiang/"
                            },
                            {
                                text: "Peep on Twitter",
                                url: "https://twitter.com/kylonkyloff"
                            }
                        ]
                    }
                />
            </Section>
        </React.Fragment>
    );
}