import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { NestedList, Listing } from "../components/nested-list";
import { ProjectLogistic } from "../components/project-logistic";
import { Subsection } from "../components/subsection";
import { ProjectCard } from "../components/project-card";
import { ImgTextDuo } from "../components/img-text-duo";
import { SoloGraphic } from "../components/solo-graphic";
import { QuoteBlock } from "../components/quote-block";
import { Example } from "../components/example";
import { IdeaGrid } from "../components/idea-grid";
import Hero from "../images/veracity/hero.png";
import HeroMobile from "../images/veracity/hero-mobile.png";
import BlueCeramicsCover from "../images/covers/blue-ceramics-cover.png";
import InfluencingFactors from "../images/veracity/influencing-factors.png";
import Mistrust from "../images/veracity/mistrust.png";
import PrimaryResearch from "../images/veracity/primary-research.png";
import ExpertInterview from "../images/shared/expert-interview.png";
import SpeedDating from "../images/veracity/speed-dating.png";
import Pretotype from "../images/veracity/pretotype.png";
import DesignSketches from "../images/veracity/design-sketches.png";
import DesignSketchesMobile from "../images/veracity/design-sketches-mobile.png";
import Evaluation from "../images/veracity/evaluation.png";
import Reflection from "../images/veracity/reflection.png";
import Actionability from "../images/veracity/actionability.png";
import Customization from "../images/veracity/customization.png";
import IdeaEvaluation from "../images/veracity/idea-evaluation.png";
import IdeaReflection from "../images/veracity/idea-reflection.png";
import IdeaActionability from "../images/veracity/idea-actionability.png";
import IdeaCustomization from "../images/veracity/idea-customization.png";

export interface VeracityProps {
    isMobile: boolean;
}

export function Veracity(props: VeracityProps) {

    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Amy Zhuang",
                "Alexandra Hopping",
                "Janelle Wen",
                "Rachel Arredondo"
            ]
        },
        {
            title: "Faculty Advisors",
            items: [
                "Anna Abovyan",
                "Raelin Musuraca"
            ]
        },
        {
            title: "PNNL Partners",
            items: [
                "Dustin Arendt",
                "Maria Glenski"
            ]
        }
    ]

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <h1>Veracity</h1>
            <Section
                title={PROJECT_NAV._overview}
            >
                <SoloGraphic
                    imgUrl={props.isMobile ? HeroMobile : Hero}
                    altText={"Rioters on January 6th, 2021 starting a fire in front of the capitol"}
                    caption={"Source: 100 Days in Appalachia"}
                    ignoreTopPadding={true}
                />
                <Subsection>
                    <p>
                        Misinformation is one of the most pervasive issues of our time, and this wicked problem presented the perfect partnership opportunity between my team at Carnegie Mellon University and the Pacific Northwest National Laboratory (PNNL).
                    </p>
                    <p>
                        This project explores the use of <em>human-AI collaboration</em> to help people and social platforms fight back against the raging infodemic.
                    </p>
                </Subsection>
                <ProjectLogistic
                    roles={
                        [
                            "Design Researcher",
                            "Product Designer",
                            "Technical Specialist"
                        ]
                    }
                    timeline={
                        [
                            "February - August 2021",
                            "(7 Months)"
                        ]
                    }
                />
            </Section>
            <Section
                title={PROJECT_NAV._challenge}
            >
                <Subsection
                    title={"influencing factors"}
                >
                    <p>
                        Despite our best efforts to be mindful about the credibility of our online engagement, the vast majority of us may still unintentionally consume and spread misinformation on social platforms
                    </p>
                    <p>
                        This dissonance is created through mixture of complex technological, psychological, and social factors within as well as outside of our own control.
                    </p>
                    <SoloGraphic
                        imgUrl={InfluencingFactors}
                        altText={"Many smaller circles surround a larger circle labeled 'misinformation'"}
                        small={true}
                    />
                </Subsection>
                <Subsection
                    title={"mistrust of platforms"}
                >
                    <p>
                        Furthermore, many people are wary of social platforms. In fact, <em>a 2021 Edelman survey reported that more than two-thirds of Americans do not trust social platforms</em>.
                    </p>
                    <p>
                        This has great implications on platform-based solutions since people have to trust that platform interventions are acting in the best interest of their user base.
                    </p>
                    <SoloGraphic
                        imgUrl={Mistrust}
                        altText={"The bond between social platforms and the public is broken"}
                        small={true}
                    />
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
            >
                <Subsection>
                    <p>
                        There's no one-size-fits-all solution for misinformation, so we believe that our main contribution is to provide design guidelines that <em>not only serve to mitigate falsehoods online, but rebuild trust with users</em>.
                    </p>
                    <IdeaGrid
                        title={"design principles"}
                        ideaList={
                            [
                                {
                                    imgUrl: IdeaEvaluation,
                                    altText: "A magnifying glass icon",
                                    label: "verify information for users"
                                },
                                {
                                    imgUrl: IdeaReflection,
                                    altText: "A light bulb icon",
                                    label: "Encourage habit awareness"
                                },
                                {
                                    imgUrl: IdeaActionability,
                                    altText: "A lightning icon",
                                    label: "Provide counter mechanisms"
                                },
                                {
                                    imgUrl: IdeaCustomization,
                                    altText: "A star icon",
                                    label: "create customized interventions"
                                }
                            ]
                        }
                    />
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._process}
            >
                <Subsection
                    title={"setting the scene"}
                >
                    <p>
                        Human-AI collaboration is not a new field, so there was plenty of existing research for our team to comb through.
                    </p>
                    <ImgTextDuo
                        imgUrl={PrimaryResearch}
                        altText={"Sample of numerous articles and papers we read for background research"}
                    >
                        <p>
                            We kickstarted the project by reading 60+ related papers and articles to learn about existing research on AI tools and trust.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={ExpertInterview}
                        altText={"Speech bubbles that connote a conversation between two parties"}
                        goofy={true}
                    >
                        <p>
                            We also interviewed experts in the field to deepen our knowledge about social, psychological, and technological factors around misinformation.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"Research through Design"}
                >
                    <p>
                        User feedback was instrumental in our goal to create interventions fight misinformation, so we designed artifacts and tested them with 100+ participants to extract key traits that resonate with users.
                    </p>
                    <ImgTextDuo
                        imgUrl={SpeedDating}
                        altText={"Sample of numerous articles and papers we read for background research"}
                    >
                        <p>
                            Speed dating revealed low trust levels with AI, but also provided examples when people didn't mind autonomous systems.
                        </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={Pretotype}
                        altText={"Speech bubbles that connote a conversation between two parties"}
                        goofy={true}
                    >
                        <p>
                            Our prototypes taught us about the tools and resources that help users feel confident in their online engagement.
                        </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"The case for AI"}
                >
                    <p>
                        This project is by no means advocating for AI as a silver bullet solution—<em>the priority should still be to foster better user behaviors and social platform practices</em>.

                    </p>
                    <p>
                        However, as we wait for technology, policy, and culture to catch up, AI presents the greatest potential for immediate positive effects given the speed at which misinformation travels.
                    </p>
                </Subsection>
                <Subsection
                    title={"Convergence Point"}
                >
                    <p>
                        Social platforms come in many flavors, and our team quickly realized that prescriptive solutions would be too rigid. Instead, we mocked up and tested a suite of concepts, then analyzed our findings to surface effective ideas.
                    </p>
                    <SoloGraphic
                        imgUrl={props.isMobile ? DesignSketchesMobile : DesignSketches}
                        altText={"Design sketches from the various ideas for interventions with sticky notes to record other ideas and notes"}
                    />
                    <Example
                        title={"Evaluation"}
                    >
                        <ImgTextDuo
                            imgUrl={Evaluation}
                            altText={"Prototype screens of in-feed veracity indicator that displays fact-check verdict on posts"}
                        >
                            <p>
                                We prototyped an in-feed veracity indicator that displays the verdict of a claim accompanied by supporting facts and resources.
                            </p>
                        </ImgTextDuo>
                        <p className={"floating-block"}>
                            When we put this idea in front of research participants, one said:
                        </p>
                        <QuoteBlock
                            text={
                                "[The indicator] would give me a heads up and then I would go and do my own research...I would go off the site and do some research on my own to learn more about it, so I like that."
                            }
                        />
                        <p>
                            This shows that <em>evaluation reminds users to be more critical of their information consumption</em>, and by extension, their engagement.
                        </p>
                    </Example>
                    <Example
                        title={"Reflection"}
                    >
                        <ImgTextDuo
                            imgUrl={Reflection}
                            altText={"A prototype with bar chart to indicate proportion of misinformation encountered during each day of social platform use"}
                        >
                            <p>
                                A separate idea contained a data dashboard that highlights engagement with misinformation over time.
                        </p>
                        </ImgTextDuo>
                        <p className={"floating-block"}>
                            Almost all participants wanted to dig further into this data. One told us:
                        </p>
                        <QuoteBlock
                            text={
                                "I would definitely look at which accounts those were and try to figure out why it was indicated to me that those sources were not credible, and the metric by which that was assessed."
                            }
                        />
                        <p>
                            This demonstrates that reflection is necessary and <em>prompts users to evaluate their engagement habits</em> to catalyze behavior change.
                        </p>
                    </Example>

                    <Example
                        title={"Actionability"}
                    >
                        <ImgTextDuo
                            imgUrl={Actionability}
                            altText={"A prototype that allows users to review information that has been verified or disproven and allows them to disengage if desired"}
                        >
                            <p>
                                Another concept provided options to remove engagement with content that was later declared false by trusted sources.
                        </p>
                        </ImgTextDuo>
                        <p className={"floating-block"}>
                            Almost all participants chose to remove their engagement. When asked about their rationale, one participant said:
                        </p>
                        <QuoteBlock
                            text={
                                "I wouldn't want to be spreading stuff that I believe to be true that really wasn't... At this point in my life, truth is important to me."
                            }
                        />
                        <p>
                            This and many similar sentiments highlight <em>the need for interventions to enable users to protect and restore online reputation</em>.
                        </p>
                    </Example>

                    <Example
                        title={"Customization"}
                    >
                        <ImgTextDuo
                            imgUrl={Customization}
                            altText={"A prototype with different content within notifications to test user reaction"}
                        >
                            <p>
                                We discovered the importance of customization in a survey test to determine effective notification strategies to engage users in interventions about COVID misinformation.
                        </p>
                        </ImgTextDuo>
                        <p className={"floating-block"}>
                            The response from participants was a mixed bag and generally fell into the two following categories:
                        </p>
                        <QuoteBlock
                            text={
                                "Sick of listening to anything about COVID."
                            }
                        />
                        <QuoteBlock
                            text={
                                "I like to stay in the loop about important things, like a pandemic. Being educated is important."
                            }
                        />
                        <p>
                            These contradictory responses underscore the importance of <em>tailoring interventions for individual users to increase likelihood of success</em>.
                        </p>
                    </Example>
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._conclusion}
            >
                <p>
                    Misinformation is a large-scale societal issue that encompasses more than social platforms, and more research needs to be done to understand the sociology behind misinformation since, at its root, misinformation is a human problem, not a machine problem.
                </p>
                <p>
                    However, there is no question that we have to take action against misinformation now, so these design principles serve as a stepping stone on the path to the discovery of a more holistic approach.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._reflection}
            >
                <p>
                    I'm grateful to have the opportunity to pore over such a complex problem that pushed me to question the role of design. This project taught me a great deal about the power of design, and I think designers need to be more critical in their practice if we want to see meaningful changes.
                </p>
                <p>
                    Lastly, many thanks to the researchers and experts who lent us their knowledge to help us create a cohesive body of work, and special gratitude to our research participants because none of this would have been possible without them.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
            >
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._blue_ceramics}`}
                    title={"Blue Ceramics"}
                    description={"Restoring seagrass meadows with morphing ceramic structures"}
                    imgUrl={BlueCeramicsCover}
                    altText={"Isometric view of mountain-like ceramic structures posed in an underwater scene with marine life and seagrass"} />
            </Section>
        </React.Fragment>
    );
}