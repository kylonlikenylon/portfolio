import * as React from "react";
import { Helmet } from "react-helmet";
import { PROJECT_NAV, PROJ_ROUTE } from "../app";
import { Section } from "../components/section";
import { Listing, NestedList } from "../components/nested-list";
import { ProjectCard } from "../components/project-card";
import { SoloGraphic } from "../components/solo-graphic";
import { Subsection } from "../components/subsection";
import { ProjectLogistic } from "../components/project-logistic";
import { ImgTextDuo } from "../components/img-text-duo";
import { Example } from "../components/example";
import { IdeaGrid } from "../components/idea-grid";
import Hero from "../images/f812/hero.png";
import HeroMobile from "../images/f812/hero-mobile.png";
import Gap from "../images/f812/gap.png";
import Emergent from "../images/f812/emergent.png";
import Research from "../images/f812/research.png";
import ExpertInterview from "../images/shared/expert-interview.png";
import Storyboards from "../images/f812/storyboards.png";
import Educate from "../images/f812/educate.png";
import Sensitive from "../images/f812/sensitive.png";
import Relatable from "../images/f812/relatable.png";
import IdeaEducate from "../images/f812/idea-educate.png";
import IdeaRelatable from "../images/f812/idea-relatable.png";
import IdeaSensitive from "../images/f812/idea-sensitive.png";
import VeracityCover from "../images/covers/veracity-cover.png";

export interface F812Props {
    isMobile: boolean;
}

export function F812(props: F812Props) {
    const thanks: Listing[] = [
        {
            title: "Collaborators",
            items: [
                "Monica Chang",
                "Steve Orchosky"
            ]
        },
        {
            title: "Faculty Advisors",
            items: [
                "Jason Hong",
                "Hong Chen"
            ]
        }
    ];

    return (
        <React.Fragment>
            <Helmet>
                <title>Kylon ✧ In Practice</title>
            </Helmet>
            <h1>F8*12</h1>
            <Section
                title={PROJECT_NAV._overview}
            >
                <SoloGraphic
                    imgUrl={props.isMobile ? HeroMobile : Hero}
                    altText={"A pixelated image that imitates how facial recognition algorithms classifies human faces"}
                    caption={"Source: Electronic Frontier Foundation"}
                    ignoreTopPadding={true}
                />
                <Subsection>
                    <p>
                        Autonomous vehicles and facial recognition use Artificial Intelligence (AI), yet most people are unaware of their underlying mechanisms, nor that these systems perpetuate biases.
                    </p>
                    <p>
                        My team at Carnegie Mellon University believed that raising awareness from an early age is crucial for fighting algorithmic bias, so we developed a guiding framework for facilitating discussions with children ages 12 and older about AI ethics.
                    </p>
                </Subsection>
                <ProjectLogistic
                    roles={
                        [
                            "Design Researcher"
                        ]
                    }
                    timeline={
                        [
                            "October - November 2020",
                            "(2 Months)"
                        ]
                    }
                />
            </Section>
            <Section
                title={PROJECT_NAV._challenge}
            >
                <Subsection
                    title={"Knowledge Gap"}
                >
                    <p>
                        The opaqueness and complexity of AI algorithms make pinpointing the source of algorithmic bias incredibly difficult. These factors exacerbate the gap between AI practitioners and laypeople, <em>leaving little common ground to discuss the nuances of algorithmic bias</em>.
                </p>
                    <SoloGraphic
                        imgUrl={Gap}
                        altText={"4 collinear circles with the third one being just an outline to illustrate the gap"}
                        small={true}
                    />
                </Subsection>
                <Subsection
                    title={"Emergent Field"}
                >
                    <p>
                        As more examples of algorithmic bias surface, <em>educators are recognizing the need to teach these topics in the classroom</em>. However, secondary schools only recently began to adopt Computer Science in their curriculums, and computer ethics is a nascent research field, so there's no well-developed educational material on AI ethics yet.
                </p>
                    <SoloGraphic
                        imgUrl={Emergent}
                        altText={"A graph that starts wide, narrows in quickly, then widens out again to evoke the feeling of emerging ideas"}
                        small={true}
                    />
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._outcome}
            >
                <p>
                    Drawing from our research and the knowledge of educators, we propose the following considerations for developing  AI ethics educational materials for secondary schools.
                </p>
                <IdeaGrid
                    title={"grounding principles"}
                    ideaList={
                        [
                            {
                                imgUrl: IdeaEducate,
                                altText: "A globe to stand for education",
                                label: "start with educators"
                            },
                            {
                                imgUrl: IdeaSensitive,
                                altText: "A heart shape with a band-aid on one side",
                                label: "be sensitive to sensitivities"
                            },
                            {
                                imgUrl: IdeaRelatable,
                                altText: "3 faces in  profile in a line, the middle one is a different color from the rest",
                                label: "Make it relatable"
                            }
                        ]
                    }
                />
            </Section>
            <Section
                title={PROJECT_NAV._process}
            >
                <Subsection
                    title={"Seeking Expertise"}
                >
                    <p>
                        No one on the team was an expert in pedagogy, but we still felt strongly about spreading awareness of algorithmic bias to the next generation, so we dove headfirst into research.
                </p>
                    <ImgTextDuo
                        imgUrl={Research}
                        altText={"Some screenshots from websites where we encountered AI educational materials"}
                    >
                        <p>
                            We analyzed existing educational materials for AI ethics and took stock of their approach to teach this difficult topic.
                    </p>
                    </ImgTextDuo>
                    <ImgTextDuo
                        imgUrl={ExpertInterview}
                        altText={"Speech bubbles that connote a conversation between two parties"}
                        goofy={true}
                    >
                        <p>
                            Then we interviewed school admins to survey how educators currently handle discussions around algorithmic bias.
                    </p>
                    </ImgTextDuo>
                    <p className={"floating-block"}>
                        Our initial research revealed that while some materials exist, there's no well-established framework for AI ethics education. Furthermore, <em>ethics is a nuanced topic more easy to get across for older children</em>, so we narrowed our target audience to ages 12+.
                    </p>
                </Subsection>
                <Subsection
                    title={"Would You Rather"}
                >
                    <ImgTextDuo
                        imgUrl={Storyboards}
                        altText={"Examples storyboards of proposed classroom exercises"}
                    >
                        <p>
                            Then we interviewed school admins to survey how educators currently handle discussions around algorithmic bias.
                    </p>
                    </ImgTextDuo>
                </Subsection>
                <Subsection
                    title={"Early Observations"}
                >
                    <p>
                        We noticed a pattern in the ideas that were well-received by participants. And while these interviews confirmed that fleshing out a complete workshop would require more time and research, we find value in sharing our learnings.
                    </p>
                    <Example
                        title={"Start with Educators"}
                        extraSpace={true}
                    >
                        <ImgTextDuo
                            imgUrl={Educate}
                            altText={"Two faces in profile face each other with a lightbulb between them"}
                        >
                            <p>
                                Provide prior training to educators to help them feel comfortable facilitating discussions around an unfamiliar topic such as AI.
                            </p>
                        </ImgTextDuo>
                    </Example>
                    <Example
                        title={"Be Sensitive to Sensitivities"}
                        extraSpace={true}
                    >
                        <ImgTextDuo
                            imgUrl={Sensitive}
                            altText={"Two faces in profile face each other with a box with a lock overlaid on top between them"}
                            goofy={true}
                        >
                            <p>
                                If personal details are required, have students engage one-on-one to avoid public humiliation.
                            </p>
                        </ImgTextDuo>
                    </Example>
                    <Example
                        title={"Make It Relatable"}
                        extraSpace={true}
                    >
                        <ImgTextDuo
                            imgUrl={Relatable}
                            altText={"A face in profile looking into a hand mirror where another face in profile looks back"}
                        >
                            <p>
                                Ground examples of algorithmic in daily life to demonstrate the ubiquity of AI in familiar places.
                            </p>
                        </ImgTextDuo>
                    </Example>
                </Subsection>
            </Section>
            <Section
                title={PROJECT_NAV._conclusion}
            >
                <p>
                    The framework presented here is by no means exhaustive, but we hope this work inspires efforts to develop and fold ethics into computer science curricula as we build our way to a more fair and just world.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._reflection}
            >
                <p>
                    During the course of this work, my views on artificial intelligence completely transformed. I've come to see that AI has the potential to benefit our world in many ways, so I now favor development with caution, as long as we prioritize education.
                </p>
                <p>
                    I want to give a shout-out to the educators who took time out of their busy lives to go through this thought experiment with us. You work to shape future generations deserves all the appreciation in the world.
                </p>
            </Section>
            <Section
                title={PROJECT_NAV._thanks}
            >
                <NestedList
                    listOfList={thanks}
                />
            </Section>
            <Section
                title={PROJECT_NAV._next}
            >
                <ProjectCard
                    route={`/${PROJ_ROUTE._root}/${PROJ_ROUTE._veracity}`}
                    title={"Veracity"}
                    description={"Mitigating the consumption and spread of misinformation "}
                    imgUrl={VeracityCover}
                    altText={"Two clay phones in isometric view showing prototypes of interventions for misinformation on social platforms"} />
            </Section>
        </React.Fragment>
    )
}