import * as React from "react";
import { Link } from "react-scroll";
import "./style.scss";

export interface NavBarProps {
    navList: string[];
    selected: string;
    scrollOffset: number;
    onClick: (item: string) => void;
}

export function NavBar(props: NavBarProps) {
    const listLength = props.navList.length;
    let lastAnchor: string | null = null;
    let secondtoLast: string | null = null;

    if (listLength >= 2) {
        lastAnchor = props.navList[listLength - 1];
        secondtoLast = props.navList[listLength - 2];
    }


    const selectedClass = "selected";

    const [selected, setSelected] = React.useState(props.selected);

    const handleSetActive = (item: string) => {
        const elem = document.getElementById(`anchor-${lastAnchor}`);
        if (elem && item !== lastAnchor) {
            elem.classList.remove(selectedClass);
        }

        setSelected(item);
    }

    const handleSetInactive = () => {
        const elem = document.getElementById(`anchor-${lastAnchor}`);

        if (selected === secondtoLast && elem) {
            elem.classList.remove(selectedClass);
        }
    }

    React.useEffect(() => {
        const scrollHandler = () => {
            const lastElem = document.getElementById(`anchor-${lastAnchor}`);
            const secondToLastElem = document.getElementById(`anchor-${secondtoLast}`);
            if (
                lastElem &&
                secondToLastElem &&
                (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2
            ) {
                lastElem.classList.add(selectedClass);
                secondToLastElem.classList.remove(selectedClass);
            }
        };

        window.addEventListener("scroll", scrollHandler);

        return () => {
            window.removeEventListener("scroll", scrollHandler);
        }
    });

    return (
        <nav>
            <ul>
                {
                    props.navList.map((item: string) => {
                        return (
                            <li
                                className={"nav-item"}
                                key={item}
                                onClick={() => { props.onClick(item) }}
                            >
                                <Link
                                    to={item}
                                    spy={true}
                                    smooth={"easeOutQuad"}
                                    duration={250}
                                    offset={-1 * props.scrollOffset}
                                    className={"anchor"}
                                    id={`anchor-${item}`}
                                    activeClass={selectedClass}
                                    onSetActive={handleSetActive}
                                    onSetInactive={handleSetInactive}
                                >
                                    <h4>{item}</h4>
                                </Link>
                            </li>
                        )
                    })
                }
            </ul>
        </nav>
    );
}