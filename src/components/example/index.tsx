import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface ExampleProps {
    title: string;
    children: React.ReactNode;
    extraSpace?: boolean;
}

export function Example(props: ExampleProps) {
    const exampleClass = classNames(
        "example",
        {
            "extra-space": props.extraSpace
        }
    )

    return (
        <div className={exampleClass}>
            <h3 className={"title"}>{props.title}</h3>
            {props.children}
        </div>
    )

}