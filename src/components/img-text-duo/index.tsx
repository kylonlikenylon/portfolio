import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface ImgTextDuoProps {
    children: React.ReactNode;
    imgUrl: string;
    altText: string;
    goofy?: boolean;
}

export function ImgTextDuo(props: ImgTextDuoProps) {
    const duoClass = classNames(
        "img-text-duo",
        {
            "goofy": props.goofy
        }
    )
    return (
        <div className={duoClass}>
            <img
                className={"img"}
                src={props.imgUrl}
                alt={props.altText}
            />
            <div className={"text"}>
                {props.children}
            </div>
        </div>
    );
}