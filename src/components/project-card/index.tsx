import * as React from "react";
import "./style.scss";
import { InternalLink } from "../internal-link";
import { Link } from "react-router-dom";

export interface ProjectCardProps {
    title: string;
    imgUrl: string;
    altText: string;
    description: string;
    route: string;
}

export function ProjectCard(props: ProjectCardProps) {
    return (
        <div
            className={"project-card"}
        >
            <Link
                to={props.route}
            >
                <img
                    className={"img"}
                    src={props.imgUrl}
                    alt={props.altText}
                />
            </Link>
            <div
                className={"text"}
            >
                <h2 className={"title"}>{props.title}</h2>
                <p className={"desc"}>{props.description}</p>
                <InternalLink
                    text={"read case study"}
                    url={props.route}
                />
            </div>
        </div>
    )
}