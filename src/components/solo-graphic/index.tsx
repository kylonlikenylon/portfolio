import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface SoloGraphicProps {
    imgUrl: string;
    altText: string;
    ignoreTopPadding?: boolean
    caption?: string;
    small?: true;
}

export function SoloGraphic(props: SoloGraphicProps) {
    const graphicClass = classNames(
        "solo-graphic",
        {
            "small": props.small,
            "ignore-top-padding": props.ignoreTopPadding
        }
    )

    return (
        <figure
            className={graphicClass}
        >
            <img
                src={props.imgUrl}
                alt={props.altText}
            />
            {
                props.caption ?
                    <figcaption>{props.caption}</figcaption>
                    : undefined
            }
        </figure>
    );
}