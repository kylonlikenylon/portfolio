import * as React from "react";
import "./style.scss";
import { ExternalLink } from "../external-link";

export interface ContactInfoProps {
    methodList: Method[];
}

export interface Method {
    text: string;
    url: string;
}

export function ContactInfo(props: ContactInfoProps) {
    return (
        <div className={"contact-info"}>
            {
                props.methodList.map((method: Method) => {
                    return (
                        <ExternalLink
                            key={method.text}
                            text={method.text}
                            url={method.url}
                        />
                    );
                })
            }
        </div>
    );
}