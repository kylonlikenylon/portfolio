import * as React from "react";
import "./style.scss";
import { ExternalLink } from "../external-link";

export interface ContentRedirectProps {
    blurb: string;
    redirectText: string;
    redirectUrl: string;
}

export function ContentRedirect(props: ContentRedirectProps) {
    return (
        <div className={"content-redirect"}>
            <p className={"blurb"}>{props.blurb}</p>
            <ExternalLink
                text={props.redirectText}
                url={props.redirectUrl}
            />
        </div>
    )
}