import * as React from "react";
import "./style.scss";

export interface ImgDuoProps {
    alphaImgUrl: string;
    alphaAltText: string;
    deltaImgUrl: string;
    deltaAltText: string;
}

export function ImgDuo(props: ImgDuoProps) {
    return (
        <div className={"img-duo"}>
            <img
                src={props.alphaImgUrl}
                alt={props.alphaAltText}
            />
            <img
                src={props.deltaImgUrl}
                alt={props.deltaAltText}
            />
        </div>
    );
}