import * as React from "react";
import "./style.scss"

export interface SubsectionProps {
    title?: string;
    children: React.ReactNode;
}

export function Subsection(props: SubsectionProps) {
    return (
        <div className={"subsection"}>
            {
                props.title ?
                    <h2 className={"title"}>{props.title}</h2>
                    : undefined
            }
            {props.children}
        </div>
    );
}