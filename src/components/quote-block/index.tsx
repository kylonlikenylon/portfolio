import * as React from "react";
import "./style.scss"

export interface QuoteBlockProps {
    text: string;
}

export function QuoteBlock(props: QuoteBlockProps) {
    return (
        <div
            className={"quote-block"}
        >
            <h1
                className={"quote-mark"}
                id={"left"}>“</h1>
            <p className={"text"}>{props.text}</p>
            <h1
                className={"quote-mark"}
                id={"right"}>”</h1>
        </div>
    )
}