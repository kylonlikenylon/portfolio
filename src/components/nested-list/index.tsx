import * as React from "react";
import "./style.scss";

export interface NestedListProps {
    listOfList: Listing[];
}

export interface Listing {
    title: string;
    items: string[];
}

export function NestedList(props: NestedListProps) {
    return (
        <div className={"nested-list"}>
            {
                props.listOfList.map((listing: Listing) => {
                    const listItems = listing.items.map((item: string) => {
                        return (
                            <li
                                key={item}
                                className={"list-item"}
                            >
                                {item}
                            </li>);
                    });

                    return (
                        <div
                            className={"listing"}
                            key={listing.title}
                        >
                            <h3 className={"list-title"}>{listing.title}</h3>
                            <ul>
                                {listItems}
                            </ul>
                        </div>
                    )
                })
            }
        </div>
    )
}