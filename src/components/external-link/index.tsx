import * as React from "react";
import "./style.scss";
import classNames from "classnames";

export interface ExternalLinkProps {
    text: string;
    url: string;
    centered?: boolean;
}

export function ExternalLink(props: ExternalLinkProps) {
    const linkClass = classNames(
        "external-link",
        {
            "centered": props.centered
        }
    )
    return (
        <a 
            className={linkClass}
            href={props.url}
            target="_blank"
        >
            <h3>{props.text}</h3>
            <div className={"arrow"} aria-hidden={true}></div>
        </a>
    );
}