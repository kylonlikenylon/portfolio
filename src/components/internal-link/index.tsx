import * as React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import "./style.scss";

export interface InternalLinkProps {
    text: string;
    url: string;
    goofy?: boolean;
}

export function InternalLink(props: InternalLinkProps) {
    const linkClass = classNames(
        "internal-link",
        {
            "goofy": props.goofy
        }
    );

    return (
        <Link
            to={props.url}
            className={linkClass}
        >
            <h4 className={"bold"}>{props.text}</h4>
            <div className={"arrow"} aria-hidden={true}></div>
        </Link>
    );
}