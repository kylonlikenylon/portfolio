import * as React from "react";
import classNames from "classnames";
import "./style.scss";

export interface IdeaGridProps {
    title: string;
    ideaList: Idea[];
    children?: React.ReactNode;
}

interface Idea {
    imgUrl: string;
    altText: string;
    label: string;
}

export function IdeaGrid(props: IdeaGridProps) {
    const isOdd = props.ideaList.length % 2 === 1;

    const bodyClass = classNames(
        "body",
        {
            "is-odd": isOdd
        }
    )

    return (
        <div
            className={"idea-grid"}
        >
            <h2 className={"title"}>{props.title}</h2>
            {
                props.ideaList.length ?
                    <div className={bodyClass}>
                        {
                            props.ideaList.map((idea: Idea, index: number) => {
                                return (
                                    <figure
                                        id={isOdd && index === 0 ? "loner" : "coupled"}
                                        className={"idea"}
                                        key={idea.label}
                                    >
                                        <img
                                            src={idea.imgUrl}
                                            alt={idea.altText}
                                        />
                                        <figcaption><h4 className={"bold"}>{idea.label}</h4></figcaption>
                                    </figure>
                                );
                            })
                        }
                    </div>
                    : undefined
            }
            {props.children ? props.children : undefined}
        </div>
    )
}