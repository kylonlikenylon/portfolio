import * as React from "react";
import { Element } from "react-scroll";
import "./style.scss";

export interface SectionProps {
    title: string;
    children: React.ReactNode
}

export function Section(props: SectionProps) {
    return (
        <Element
            name={props.title}
            className={"content-section"}
            id={props.title}
        >
            <h4 className={"section-header bold"}>{props.title.replace("-", " ")}</h4>
            {props.children}
        </Element>
    );
}