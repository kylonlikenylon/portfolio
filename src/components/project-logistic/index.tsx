import * as React from "react";
import { NestedList } from "../nested-list";
import "./style.scss";

export interface ProjectLogisticProps {
    roles: string[];
    timeline: string[];
}

export function ProjectLogistic(props: ProjectLogisticProps) {
    return (
        <div className="project-logistic">
            <NestedList
                listOfList={
                    [
                        {
                            title: "Roles",
                            items: [...props.roles]
                        },
                        {
                            title: "Timeline",
                            items: [...props.timeline]
                        }
                    ]
                }
            />
        </div>
    );
}