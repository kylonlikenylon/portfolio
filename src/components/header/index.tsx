import * as React from "react";
import { Link } from "react-router-dom";
import { InternalLink } from "../internal-link";
import Logo from "../../images/illustrations/logo.svg";
import "./style.scss";

export interface HeaderProps {
    goesBack: boolean;
}

export function Header(props: HeaderProps) {
    return (
        <header>
            <Link
                to={"/"}
            >
                <img
                    src={Logo}
                    alt={"Logo with Kylon's name set in children's letter blocks"}
                />
            </Link>
            {
                props.goesBack ?
                    <InternalLink
                        text={"go home"}
                        url={"/"}
                        goofy={true}
                    />
                    : undefined
            }
        </header>
    )
}