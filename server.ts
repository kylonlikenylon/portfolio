import express from "express";
import path from "path";

const server = express();
const servePath = path.join(__dirname, "build");
const port = process.env.PORT || 3000;

server.use(express.static(servePath));
server.set("port", port);

server.get('*', (req, res) => {
    res.sendFile(path.join(servePath, "index.html"));
 });

server.listen(server.get("port"), () => {
    console.log("App running on port ", port);
});